# resource request table

| Request | buffer size | Service type| Notes |
| :-----:  | :-------:  |  :---------| :------|
| -   | 0 | - |Buffer empty initially |
| `3` |  1 | Not Immediate | `3` was not in the buffer |
| `2` |  2 | Not Immediate ||
| `0` |  3| Not Immediate ||
| `3` | 3 | Immediate | `3` was in the buffer |
| `1` | 3 | Not Immediate |`1` was not in the buffer <br/> `2` removed from the buffer|
|`0` | 3 | Immediate | `0` was in the buffer|


